/******************/
/*Using libraries*/
/****************/
#include <Servo.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <IRremote.h>

/******************/
/* Water Level    */
/******************/
#define watherPin A2
#define AC_WATER_PUMP 7

/******************/
/* Temp Defines */
/******************/
#define ONE_WIRE_BUS 2

int lightlimit = 0.20 * 100 ;
int templimit = 20 ;
int watherlimit = 30 ;
int RECV_PIN = 3;

//give a name to the servo
Servo servoLR;

//Set the LCD I2C address
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

//IR
IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {
  lcd.begin(20, 4); 
  pinMode(7, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT); 
  servoLR.attach(8);
  servoLR.write(0);
  sensors.begin();
  irrecv.enableIRIn(); // Start the receiver
}

/******************/
/* Remote Defines */
/******************/
#define plusbutton 0xFFA857
#define minusbutton 0xFFE01F
#define onebutton 0xFF30CF
#define twobutton 0xFF18E7
#define threebutton 0xFF7A85
#define zerobutton 0xFF6897


/******************/
/* Check number  */
/******************/


void irchak() {
  lcd.setCursor(0, 1);
  lcd.print("*Select number 1-3* ");
  if (irrecv.decode(&results)) {
    irrecv.resume();
    delay(200);//delay for relesing previous button
    if (results.value == onebutton) {
      lcd.setCursor(0, 1);
      lcd.print("Light Lim: ");
      lcd.print(lightlimit);
      lcd.print(" ??");
      IRplusminus(&lightlimit);
      lcd.setCursor(0, 1);
      lcd.print("Light Lim: ");
      lcd.print(lightlimit);
      lcd.print("   ");
    }
    if (results.value == twobutton) {
      lcd.setCursor(0, 1);
      lcd.print("Temp Lim: ");
      lcd.print(templimit);
      lcd.print(" ??");
      IRplusminus(&templimit);
      lcd.setCursor(0, 1);
      lcd.print("Temp Lim: ");
      lcd.print(templimit);
      lcd.print("   ");
    }
    if (results.value == threebutton) {
      lcd.setCursor(0, 1);
      lcd.print("water Lim: ");
      lcd.print(watherlimit);
      lcd.print(" ??");
      IRplusminus(&watherlimit);
      lcd.setCursor(0, 1);
      lcd.print("water Lim: ");
      lcd.print(watherlimit);
      lcd.print("   ");
    }

    lcd.setCursor(0, 1);
    delay(3000);
    lcd.print("*Select number 1-3* ");
  }

}



/******************/
/* plus minus */
/******************/
void IRplusminus(int *limit ) {

  lcd.setCursor(0, 1);
  lcd.print("Click + - to control");
  //Serial.print("limit is: ");
  //Serial.println(*limit);
  
  while ((!irrecv.decode(&results)) && !(results.value == plusbutton) && !(results.value == minusbutton) )
  {
    delay(50);
  }
  irrecv.resume();
  //Serial.println("Key was pressed");
  if (results.value == plusbutton) {
    //Serial.print("+");
    *limit = *limit + 1;
  }
  else if (results.value == minusbutton) {
    //Serial.print("-");
    *limit = *limit - 1;
  }
  else {
    *limit = *limit;
  }
  delay(3000);
  lcd.setCursor(0, 1);
  lcd.print("Click + - to control");
  //Serial.print("limit is: ");
  //Serial.println(*limit);
}

/******************/
/* PhotoSensor    */
/******************/
#define inPhotoSensor A1
#define outPhotoSensor A0
#define no_In_Light_No_Out_Light 0
#define yes_In_Light_Yes_Out_Light 1
#define no_In_Light_Yes_Out_Light 2
#define yes_In_Light_No_Out_Light 3
#define Emergencylight 5

 void ServoWrite(int state){
  servoLR.write(state);
 }

void roomLight(int state)
{
  digitalWrite(4, state);
}


int lightSensorValue(int Sensor)
{
  float x = analogRead(Sensor) * 0.0048;
  return x*100;
}

byte lightChck(){
  int inSensor, outSensor;
  inSensor = lightSensorValue(inPhotoSensor);
  outSensor = lightSensorValue(outPhotoSensor);
  
  if (outSensor < lightlimit) {

       if (inSensor < lightlimit) {

            return yes_In_Light_Yes_Out_Light; //1
    }

       else {

            return no_In_Light_Yes_Out_Light;  //2
    }
  }

  else{
    
       if (inSensor < lightlimit){
        
            return yes_In_Light_No_Out_Light; //3
    }
        else{
      
             return no_In_Light_No_Out_Light; // Emergency lighting is on //0
    }
  }

}



/************************/
/* Servo angle Defines */
/**********************/
#define servo_angle_open 90
#define servo_angle_closed 0


void lightgo() { 
  roomLight(HIGH);
  switch (lightChck())
  {

    
    case no_In_Light_No_Out_Light:
      roomLight(LOW);
      ServoWrite(servo_angle_closed);
      digitalWrite(Emergencylight,HIGH);
      lcd.setCursor(0,0);
      lcd.print("no in on out        ");
      break;

      
    case yes_In_Light_Yes_Out_Light:
      roomLight(LOW);
      ServoWrite(servo_angle_open);
      digitalWrite(Emergencylight,LOW);
      lcd.setCursor(0,0);
      lcd.print("yes in yes out      ");
      break;

      
    case no_In_Light_Yes_Out_Light:
      roomLight(LOW);
      ServoWrite(servo_angle_open);
      digitalWrite(Emergencylight,LOW);
      lcd.setCursor(0,0);
      lcd.print("no in yes out       ");
      break;

      
    case yes_In_Light_No_Out_Light:
      roomLight(HIGH);
      ServoWrite(servo_angle_open);
      digitalWrite(Emergencylight,LOW);
      lcd.setCursor(0,0);
      lcd.print("yes in no out       ");
      break;
  }
}


void tempchak() {
  sensors.requestTemperatures();  // Send the command to get temperatures
  int temp = sensors.getTempCByIndex(0);
  lcd.setCursor(0, 2);
  if (temp > templimit) {
    lcd.print("too hot=");
  }
  
  else if (temp < templimit) {
    lcd.print("too cold=");
  }
  
  lcd.print(temp);
  lcd.print(" C");
  lcd.print("                 ");
}


int convertToPercent() {
  float val ;
  int percent;
  val = analogRead(watherPin);
  percent = (val * 100) / 1024;
  return percent;
}

void wathering() {
  int state ;
  
  if (convertToPercent() > watherlimit){
    digitalWrite(AC_WATER_PUMP,1);
    state =1;
}
  else{
  digitalWrite(AC_WATER_PUMP,0);  
  state =0;
  }
  
lcd.setCursor(0,3);
lcd.print("pump=");
lcd.print(state);

}


void loop() {
  irchak();
  lightgo();
  wathering();
  tempchak();
}